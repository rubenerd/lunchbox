#!/bin/sh

######
## download-audioboom.sh
## Script to download audio from Audioboom
##

_URL=$1

if [ $# -ne 1 ]; then
	printf "%s\n" "Usage: ./download-audioboom.sh <PROFILE URL>"
	exit 1
fi

######
## Rule 0: Don't write your own HTML parser!
## Rule 1: Cough

_PAGES=$(curl $_URL | egrep -o 'https://audioboom.com/posts[^>"]+' | sort -u)

for _PAGE in $_PAGES; do
	printf '%s\n URL found:' "$_PAGE"
	youtube-dl -f 0 "$_PAGE"
done

