#!/bin/sh

######
## Install Minecraft server on fresh FreeBSD VM
## TODO: Add to my Ansible modules

## Create 
zfs create zroot/minecraft
mkdir /minecraft/db/
mkdir /minecraft/etc/
mkdir /minecraft/log/

## Get packages
if [ ! -f "/usr/local/etc/pkg/repos/FreeBSD.conf" ]; then
    cp /etc/pkg/FreeBSD.conf /usr/local/etc/pkg/repos/
    sed -i '' -e 's/quarterly/latest/g' /usr/local/etc/pkg/repos/FreeBSD.conf
    export ASSUME_ALWAYS_YES=YES
    pkg bootstrap -f
fi

## Install dependencies
pkg update
pkg install dialog4ports openjdk8 portmaster tmux

## Add required OpenJDK FSs
mount -t fdescfs fdesc /dev/fd
mount -t procfs proc /proc
printf "%s\n" "fdesc                   /dev/fd         fdescfs rw             0       0" >> /etc/fstab
printf "%s\n" "proc                    /proc           procfs  rw             0       0" >> /etc/fstab

## Grab ports tree
if [ ! -f "/usr/ports/GIDs" ]; then
    svnlite checkout https://svn.FreeBSD.org/ports/head/ /usr/ports/
else
    cd /usr/ports
    svnlite update
fi

## Install
export OPTIONS_SET=DAEMON
export OPTIONS_UNSET=STANDALONE

portmaster -y games/minecraft-server

