#!/bin/sh

####
## My idea zpool configuration for data
## Provide the volume name, and 1 or more drives
##
## ashift=12                    blocksize for modern drives (yes, -o not -O)
## atime=off                    don't need access times
## exec=off                     don't need executables in data drives
## compression=lz4              lightning-fast compression
## utf8only=on                  no (good) reason to not use utf8 now
## normalization=formD          best normalisation I've found for CJK
## casesensitivity=insensitive  plays well with macOS, Windows

if [ "$#" -lt 2 ]; then
	printf "%s\n" "Usage: ./ideal-data-zpool.sh <VOLUME NAME> <DRIVES>"
	exit 1
fi

zpool create \
	-o ashift=12 \
	-O atime=off \
	-O exec=off \
	-O compression=lz4 \
	-O utf8only=on \
	-O normalization=formD \
	-O casesensitivity=insensitive 
	"$@"

