#!/usr/bin/env perl

######
## xml.pl
## Simple XML parsing examples using XML::LibXML
## 2021-05

use strict;
use warnings;
use autodie;
binmode(STDOUT, ':utf8'); 
use feature qw(say);

## This is discoraged by XML::LibXML
## use open qw(:std :utf8);

use LWP;
use LWP::UserAgent;
use URI;
use XML::LibXML;

#####################################################################

sub main {
	say '==> Parsing inline XML...';

	my $source = qq(<?xml version="1.0" encoding="utf-8"?>
		<urlset>
			<url>
				<loc>http://rubenerd.com</loc>
		</url>
	</urlset>);

	my $xml = XML::LibXML->load_xml(string => $source);

	foreach my $url ($xml->findnodes('/urlset/url/loc')) {
		say $url->to_literal();
	}

	#################################################################

	say '==> Parsing XML from remote HTTP...';

	my $rss = URI->new('http://showfeed.rubenerd.com/');
	$xml = XML::LibXML->load_xml(location => $rss);

	#foreach my $title ($xml->findnodes('/rss/channel/item/title')) {
	foreach my $title ($xml->findnodes('//item/title')) {
		say $title->to_literal();
	}

	#################################################################

	say '==> Parsing XML from remote HTTPS, with wide chars...';

	$rss = URI->new('https://rubenerd.com/feed/');
	my $ua = LWP::UserAgent->new();
	my $req = HTTP::Request->new(GET => $rss);
	my $content = $ua->request($req)->content();

	## If you just want to use LWP::Simple:
	## my $content = get($rss);

	$xml = XML::LibXML->load_xml(string => $content);

	foreach my $title ($xml->findnodes('/rss/channel/item/title')) {
		say $title->to_literal();
	}

	#################################################################

	say '==> Parsing from file';

	my $file = 'server.xml';
	open(my $fh, '<', $file) or die "Could not open file $file, $!";

	$xml = XML::LibXML->load_xml(IO => $fh);

	foreach my $connector ($xml->findnodes('///@port')) {
		say "Port ". $connector->to_literal();
	}

	close($fh);

	return 0;
}

exit(&main());

